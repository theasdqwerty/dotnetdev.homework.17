using System;
using System.Collections.Generic;
using System.Linq;

namespace dotnetcore.Extensions;

public static class GetMaxExtension
{
    public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T : class
        => e.MaxBy(o => getParameter(o));
}