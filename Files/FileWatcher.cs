using System;
using System.Data.Common;
using System.IO;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace dotnetcore.Files;

public class FileWatcher
{
    public delegate void FileWatcherEventHandler(FileWatcher sender, FileArgs fileArgs);
    
    /// <summary>
    /// Событие.
    /// </summary>
    public event FileWatcherEventHandler FileFound;

    /// <summary>
    /// Маркер отмены операции.
    /// </summary>
    private CancellationTokenSource cancellationTokenSource;

    public FileWatcher(CancellationTokenSource cancellationTokenSource)
    {
        this.cancellationTokenSource = cancellationTokenSource;
    }

    /// <summary>
    /// Отмена поиска файлов.
    /// </summary>
    public void CancelSearch() 
        => cancellationTokenSource.Cancel();

    /// <summary>
    /// Поиск файлов.
    /// </summary>
    /// <param name="folderPath">Директория поиска файлов.</param>
    public async Task Run(string folderPath)
    {
        foreach (var filePath in Directory.GetFiles(folderPath))
        {
            if (cancellationTokenSource.IsCancellationRequested)
                return;
            
            // Имитация долгой работы.
            await Task.Delay(1000);
            
            FileFound?.Invoke(this, new FileArgs(Path.GetFileName(filePath)));
        }
    }
}