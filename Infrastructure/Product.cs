using System;

namespace dotnetcore.Infrastructure;

/// <summary>
/// Продукт
/// </summary>
public class Product
{
    public Product(string name, float price, DateTimeOffset dateOfManufacture)
    {
        Name = name;
        Price = price;
        DateOfManufacture = dateOfManufacture;
    }
    
    /// <summary>
    /// Наименование
    /// </summary>
    public string Name { get; set; }
    
    /// <summary>
    /// Цена 
    /// </summary>
    public float Price { get; set; }
    
    /// <summary>
    /// Дата производства
    /// </summary>
    public DateTimeOffset DateOfManufacture { get; set; }

    public override string ToString() 
        => $"Name: {Name} Price: {Price}";
}