﻿using System;
using System.Linq;
using System.Threading;
using dotnetcore.Extensions;
using dotnetcore.Files;
using dotnetcore.Infrastructure;

Console.WriteLine("Otus homework 17");

var products = new[]
{
    new Product("Beer", 150, DateTimeOffset.Now),
    new Product("Vodka", 700, DateTimeOffset.Now),
    new Product("Wisky", 1500, DateTimeOffset.Now),
    new Product("Rom", 900, DateTimeOffset.Now),
    new Product("Martiny", 800, DateTimeOffset.Now),
    new Product("Samogon", 60, DateTimeOffset.Now)
};

// 1. Обобщенная функция расширения.
// 5. Вывод результата поиска максимального значения.
Console.WriteLine("All products");
Console.WriteLine(string.Join(Environment.NewLine, products.Select(p => p)));
Console.WriteLine($"Expensive product {products.GetMax(p => p.Price)}");

// 4. Возможность отмены операции поиска файлов. Используем маркер отмены операции.
using var cts = new CancellationTokenSource();

// 2. Класс обходящий каталог файлов и выдающий событие при нахождении каждого файла.
var fileWatcher = new FileWatcher(cts);

fileWatcher.FileFound += delegate(FileWatcher sender, FileArgs eventArgs)
{
    // Возможность отмены дальнейшего поиска из обработчика.
    Console.WriteLine("Cancel operation \"yes\" to cancel \"no\" to continue?");
    var result = Console.ReadLine() ?? string.Empty;
    if (result is "yes")
    {
        Console.WriteLine("Operation was cancelled.");
        
        // Отменяем операцию.
        sender.CancelSearch();
        return;
    }

    // Вывод имени найденного файла.
    Console.WriteLine($"File found {eventArgs.FileName}.");
};


await fileWatcher.Run(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));